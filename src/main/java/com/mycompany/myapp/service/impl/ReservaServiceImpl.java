package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.Cliente;
import com.mycompany.myapp.domain.Material;
import com.mycompany.myapp.domain.RegistroMaterialUtilizado;
import com.mycompany.myapp.domain.Reserva;
import com.mycompany.myapp.repository.MaterialRepository;
import com.mycompany.myapp.repository.RegistroMaterialUtilizadoRepository;
import com.mycompany.myapp.repository.ReservaRepository;
import com.mycompany.myapp.repository.specification.ReservaSpecification;
import com.mycompany.myapp.service.ReservaService;
import com.mycompany.myapp.service.dto.ReservaDTO;
import com.mycompany.myapp.service.mapper.ReservaMapper;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Reserva}.
 */
@Service
@Transactional
public class ReservaServiceImpl implements ReservaService {

    private final Logger log = LoggerFactory.getLogger(ReservaServiceImpl.class);

    private final ReservaRepository reservaRepository;

    private final ReservaMapper reservaMapper;

    private final RegistroMaterialUtilizadoRepository registroRepository;

    private final MaterialRepository materialRepository;

    public ReservaServiceImpl(
        ReservaRepository reservaRepository,
        ReservaMapper reservaMapper,
        RegistroMaterialUtilizadoRepository registroRepository,
        MaterialRepository materialRepository
    ) {
        this.reservaRepository = reservaRepository;
        this.reservaMapper = reservaMapper;
        this.registroRepository = registroRepository;
        this.materialRepository = materialRepository;
    }

    @Override
    public ReservaDTO save(ReservaDTO reservaDTO) {
        log.debug("Request to save Reserva : {}", reservaDTO);
        Reserva reserva = reservaMapper.toEntity(reservaDTO);
        // Obtenemos los materiales de la reserva y los guardamos en una lista
        List<Material> materiales = reserva.getMateriales();
        if (materiales != null) {
            for (Material material : materiales) {
                // Compruebo que la cantidad reservada sea mayor que 0 para crear un nuevo registro
                if (material.getCantidadReservada() != 0) {
                    RegistroMaterialUtilizado registro = new RegistroMaterialUtilizado();
                    // Material material = new Material();
                    Cliente cliente = reserva.getCliente();
                    if (cliente != null) {
                        // Añadimos los datos del nuevo registro
                        registro.setReserva(reserva);
                        registro.setMaterial(material);
                        registro.setCantidad(material.getCantidadReservada());
                        registro.setNombre(cliente.getNombre());
                        // Restamos la cantidad reservada a la cantidad disponible de materiales
                        material.setCantidadDisponible(material.getCantidadDisponible() - material.getCantidadReservada());
                        // Guardamos los datos en la base de datos
                        materialRepository.save(material);
                        registroRepository.save(registro);
                    }
                }
            }
        }
        reserva.setCliente(reserva.getCliente());
        reserva.setTotal(reserva.getTotal());
        reserva.setInstalacion(reserva.getInstalacion());
        reserva = reservaRepository.save(reserva);
        return reservaMapper.toDto(reserva);
    }

    @Override
    public ReservaDTO update(ReservaDTO reservaDTO) {
        log.debug("Request to save Reserva : {}", reservaDTO);
        Reserva reserva = reservaMapper.toEntity(reservaDTO);
        List<Material> materiales = reserva.getMateriales();
        // Comprobamos que la lista de materiales no este vacia
        if (materiales.size() != 0) {
            List<Long> registrosId = reserva.getRegistrosId();
            // Se comprueba si el cliente tenia un registro
            if (registrosId.size() != 0) {
                Optional<Reserva> reservaAntiguaOptional = reservaRepository.findById(reserva.getId());
                if (reservaAntiguaOptional.isPresent()) {
                    Reserva reservaAntigua = reservaAntiguaOptional.get();
                    // Condicion para comporbar que si la instalacion reservada ha cambiado
                    if (reservaAntigua.getInstalacion().getId() == reserva.getInstalacion().getId()) {
                        // Bucle para editar o crear cada registro con la nueva informacion
                        for (Long id : registrosId) {
                            Optional<RegistroMaterialUtilizado> registroOptional = registroRepository.findById(id);
                            // Comprobamos que el registro existe en la base de datos
                            if (registroOptional.isPresent()) {
                                RegistroMaterialUtilizado registro = registroOptional.get();
                                Material materialAntiguo = registro.getMaterial();
                                // Recorremos la lista de materiales que contienen la nueva informacion
                                for (Material materialNuevo : materiales) {
                                    // Obtenemos los datos del material antiguo y los comparamos con los nuevos datos material
                                    if (materialAntiguo.getId() == materialNuevo.getId()) {
                                        // Comprobamos que la cantidad reservada del material antiguo y el nuevo sea diferente
                                        if (materialAntiguo.getCantidadReservada() != materialNuevo.getCantidadReservada()) {
                                            // Condicion para eliminar el registro si la cantidad reservada es 0 o editarlo si es diferente de 0
                                            if (materialNuevo.getCantidadReservada() == 0) {
                                                // Se suma la cantidad que se ha editado a la cantidad disponible y se borra el registro
                                                materialNuevo.setCantidadDisponible(
                                                    materialNuevo.getCantidadDisponible() +
                                                    (materialAntiguo.getCantidadReservada() - materialNuevo.getCantidadReservada())
                                                );
                                                materialRepository.save(materialNuevo);
                                                registroRepository.deleteById(registro.getId());
                                            } else {
                                                // Condicion para sumar o restar a la cantidad disponible de materiales
                                                if (materialNuevo.getCantidadReservada() < materialAntiguo.getCantidadReservada()) {
                                                    materialNuevo.setCantidadDisponible(
                                                        materialNuevo.getCantidadDisponible() +
                                                        (materialAntiguo.getCantidadReservada() - materialNuevo.getCantidadReservada())
                                                    );
                                                } else {
                                                    materialNuevo.setCantidadDisponible(
                                                        materialNuevo.getCantidadDisponible() -
                                                        (materialNuevo.getCantidadReservada() - materialAntiguo.getCantidadReservada())
                                                    );
                                                }
                                                // Editamos los datos y los guardamos en la base de datos
                                                registro.setCantidad(materialNuevo.getCantidadReservada());
                                                materialRepository.save(materialNuevo);
                                                registroRepository.save(registro);
                                            }
                                        }
                                    } else {
                                        if (materialNuevo.getCantidadReservada() != 0) {
                                            RegistroMaterialUtilizado nuevoRegistro = new RegistroMaterialUtilizado();
                                            // Material material = new Material();
                                            Cliente cliente = reserva.getCliente();
                                            if (cliente != null) {
                                                // Añadimos los datos del nuevo registro
                                                nuevoRegistro.setReserva(reserva);
                                                nuevoRegistro.setMaterial(materialNuevo);
                                                nuevoRegistro.setCantidad(materialNuevo.getCantidadReservada());
                                                nuevoRegistro.setNombre(cliente.getNombre());
                                                // Restamos la cantidad reservada a la cantidad disponible de materiales
                                                materialNuevo.setCantidadDisponible(
                                                    materialNuevo.getCantidadDisponible() - materialNuevo.getCantidadReservada()
                                                );
                                                // Guardamos los datos en la base de datos
                                                materialRepository.save(materialNuevo);
                                                registroRepository.save(registro);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // Recorremos la lista de materiales que contienen la nueva informacion
                        for (Material materialNuevo : materiales) {
                            if (materialNuevo.getCantidadReservada() != 0) {
                                RegistroMaterialUtilizado nuevoRegistro = new RegistroMaterialUtilizado();
                                // Material material = new Material();
                                Cliente cliente = reserva.getCliente();
                                if (cliente != null) {
                                    // Añadimos los datos del nuevo registro
                                    nuevoRegistro.setReserva(reserva);
                                    nuevoRegistro.setMaterial(materialNuevo);
                                    nuevoRegistro.setCantidad(materialNuevo.getCantidadReservada());
                                    nuevoRegistro.setNombre(cliente.getNombre());
                                    // Restamos la cantidad reservada a la cantidad disponible de materiales
                                    materialNuevo.setCantidadDisponible(
                                        materialNuevo.getCantidadDisponible() - materialNuevo.getCantidadReservada()
                                    );
                                    // Guardamos los datos en la base de datos
                                    materialRepository.save(materialNuevo);
                                    registroRepository.save(nuevoRegistro);
                                }
                            }
                        }
                        // Se eliminan los registros de los materiales de la instalacion anterior y se crean los nuevos registros
                        for (Long registroId : registrosId) {
                            Optional<RegistroMaterialUtilizado> registroOptional = registroRepository.findById(registroId);
                            if (registroOptional.isPresent()) {
                                RegistroMaterialUtilizado registro = registroOptional.get();
                                Material materialAntiguo = registro.getMaterial();
                                materialAntiguo.setCantidadDisponible(materialAntiguo.getCantidadDisponible() + registro.getCantidad());
                                materialAntiguo.setCantidadReservada(materialAntiguo.getCantidadReservada() - registro.getCantidad());
                                materialRepository.save(materialAntiguo);
                                registroRepository.deleteById(registro.getId());
                            }
                        }
                    }
                }
            }
        }
        reservaRepository.save(reserva);
        return reservaMapper.toDto(reserva);
    }

    @Override
    public Optional<ReservaDTO> partialUpdate(ReservaDTO reservaDTO) {
        log.debug("Request to partially update Reserva : {}", reservaDTO);

        return reservaRepository
            .findById(reservaDTO.getId())
            .map(existingReserva -> {
                reservaMapper.partialUpdate(existingReserva, reservaDTO);

                return existingReserva;
            })
            .map(reservaRepository::save)
            .map(reservaMapper::toDto);
    }

    // Funcion para obtener los registros que pertenecen a una reserva
    @Override
    @Transactional(readOnly = true)
    public Page<ReservaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Reservas");
        return reservaRepository.findAll(pageable).map(reservaMapper::toDto);
    }

    // Funcion para mostrar los registros que pertenecen a una reserva en el componenente prinicpal de reservas
    @Override
    @Transactional(readOnly = true)
    public Page<ReservaDTO> findAllForRegistro(Pageable pageable) {
        log.debug("Request to get all Reservas");
        Page<Reserva> reservas = reservaRepository.findAll(pageable);

        for (Reserva reserva : reservas) {
            Long id = reserva.getId();
            Optional<List<RegistroMaterialUtilizado>> reservaOptional = registroRepository.findByReserva(id);
            if (reservaOptional.isPresent()) {
                List<RegistroMaterialUtilizado> reservaRegistro = reservaOptional.get();
                if (reservaRegistro.size() > 0) {
                    List<Long> registros = new ArrayList<>();

                    for (RegistroMaterialUtilizado reg : reservaRegistro) {
                        registros.add(reg.getId());
                    }
                    reserva.setRegistrosId(registros);
                }
            }
        }
        return reservas.map(reservaMapper::toDto);
    }

    // Funcion para buscar una reserva por un parametro determinado
    @Override
    @Transactional(readOnly = true)
    public Page<ReservaDTO> findBySearchingParam(String filtro, Pageable pageable) {
        log.debug("Request to get all Ventas filtered");
        return reservaRepository.findAll(ReservaSpecification.searchingParam(filtro), pageable).map(reservaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ReservaDTO> findFromToday(Instant inicio, Instant fin, Pageable pageable) {
        log.debug("Request to get all Instalacions");
        Page<Reserva> reservas = reservaRepository.findByFecha(inicio, fin, pageable);
        return reservas.map(reservaMapper::toDto);
    }

    // Funcion para obtener un registro con sus materiales y registros
    @Override
    @Transactional(readOnly = true)
    public Optional<ReservaDTO> findOne(Long id) {
        log.debug("Request to get Reserva : {}", id);
        Optional<Reserva> reservaOptional = reservaRepository.findById(id);
        if (reservaOptional.isPresent()) {
            Reserva reserva = reservaOptional.get();
            Optional<List<RegistroMaterialUtilizado>> registrosOptional = registroRepository.findByReserva(id);
            // Se comprueba si la reserva tiene registros
            if (registrosOptional.isPresent()) {
                List<RegistroMaterialUtilizado> registros = registrosOptional.get();
                List<Material> materiales = new ArrayList<>();
                List<Long> registrosId = new ArrayList<>();
                // Se comprueba los materiales que le pertenecen a cada registro
                for (RegistroMaterialUtilizado reg : registros) {
                    Material m = reg.getMaterial();
                    materiales.add(m);
                    registrosId.add(reg.getId());
                }
                // Se le añaden los datos a la reserva
                reserva.setMateriales(materiales);
                reserva.setRegistrosId(registrosId);
                Optional<Reserva> resultado = Optional.of(reserva);
                return resultado.map(reservaMapper::toDto);
            }
            // return reserva.map(reservaMapper::toDto);
        }
        return reservaOptional.map(reservaMapper::toDto);
    }

    // Funcion para eliminar una reserva con sus registros y aumentar la cantidad de materiales disponible
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Reserva : {}", id);
        Optional<List<RegistroMaterialUtilizado>> registrosOptional = registroRepository.findByReserva(id);
        if (registrosOptional.isPresent()) {
            List<RegistroMaterialUtilizado> registros = registrosOptional.get();
            for (RegistroMaterialUtilizado registro : registros) {
                Material material = registro.getMaterial();
                material.setCantidadDisponible(material.getCantidadDisponible() + registro.getCantidad());
                materialRepository.save(material);
                registroRepository.deleteById(registro.getId());
            }
        }
        reservaRepository.deleteById(id);
    }
}
