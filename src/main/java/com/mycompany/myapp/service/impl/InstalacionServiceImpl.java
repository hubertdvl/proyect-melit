package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.Instalacion;
import com.mycompany.myapp.domain.Reserva;
import com.mycompany.myapp.repository.InstalacionRepository;
import com.mycompany.myapp.repository.ReservaRepository;
import com.mycompany.myapp.service.InstalacionService;
import com.mycompany.myapp.service.dto.InstalacionDTO;
import com.mycompany.myapp.service.mapper.InstalacionMapper;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.standard.expression.Each;

/**
 * Service Implementation for managing {@link Instalacion}.
 */
@Service
@Transactional
public class InstalacionServiceImpl implements InstalacionService {

    private final Logger log = LoggerFactory.getLogger(InstalacionServiceImpl.class);

    private final InstalacionRepository instalacionRepository;

    private final InstalacionMapper instalacionMapper;

    private final ReservaRepository reservaRepository;

    public InstalacionServiceImpl(
        InstalacionRepository instalacionRepository,
        InstalacionMapper instalacionMapper,
        ReservaRepository reservaRepository
    ) {
        this.instalacionRepository = instalacionRepository;
        this.instalacionMapper = instalacionMapper;
        this.reservaRepository = reservaRepository;
    }

    @Override
    public InstalacionDTO save(InstalacionDTO instalacionDTO) {
        log.debug("Request to save Instalacion : {}", instalacionDTO);
        Instalacion instalacion = instalacionMapper.toEntity(instalacionDTO);
        instalacion = instalacionRepository.save(instalacion);
        return instalacionMapper.toDto(instalacion);
    }

    @Override
    public Optional<InstalacionDTO> partialUpdate(InstalacionDTO instalacionDTO) {
        log.debug("Request to partially update Instalacion : {}", instalacionDTO);

        return instalacionRepository
            .findById(instalacionDTO.getId())
            .map(existingInstalacion -> {
                instalacionMapper.partialUpdate(existingInstalacion, instalacionDTO);

                return existingInstalacion;
            })
            .map(instalacionRepository::save)
            .map(instalacionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<InstalacionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Instalacions");
        return instalacionRepository.findAll(pageable).map(instalacionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<InstalacionDTO> findAvailable(Instant inicio, Instant fin, Pageable pageable) {
        log.debug("Request to get all Instalacions");
        Page<Reserva> reservas = reservaRepository.findByFecha(inicio, fin, pageable);
        Page<Instalacion> instalaciones = instalacionRepository.findAll(pageable);
        List<Instalacion> instalacionesAux = new ArrayList<>();
        Iterator<Instalacion> i = instalacionesAux.iterator();

        for (Instalacion instalacion : instalaciones) {
            instalacionesAux.add(instalacion);
        }

        for (Iterator<Instalacion> iterator = instalacionesAux.iterator(); iterator.hasNext();) {
            Instalacion instalacion = iterator.next();
            for (Reserva reserva : reservas) {
                if (instalacion.getId() == reserva.getInstalacion().getId()) {
                    iterator.remove();
                    break;
                }
            }
        }
        Page<Instalacion> instalacionesDisponibles = new PageImpl<Instalacion>(instalacionesAux, pageable, instalacionesAux.size());
        return instalacionesDisponibles.map(instalacionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<InstalacionDTO> findOne(Long id) {
        log.debug("Request to get Instalacion : {}", id);
        return instalacionRepository.findById(id).map(instalacionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Instalacion : {}", id);
        instalacionRepository.deleteById(id);
    }
}
