package com.mycompany.myapp.repository.specification;

import com.mycompany.myapp.domain.Material;
import com.mycompany.myapp.domain.RegistroMaterialUtilizado;
import com.mycompany.myapp.domain.Reserva;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RegistroSpecification extends JpaSpecificationExecutor<RegistroMaterialUtilizado> {
    public static Specification<RegistroMaterialUtilizado> searchingParam(String filter) {
        return new Specification<RegistroMaterialUtilizado>() {
            private static final long serialVersionUID = 1L;

            public Predicate toPredicate(Root<RegistroMaterialUtilizado> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                query.distinct(true);
                List<Predicate> ors = new ArrayList<Predicate>();

                Expression<String> nombre = root.get("nombre").as(String.class);
                Expression<String> cantidad = root.get("cantidad").as(String.class);
                Expression<String> fecha = root.get("fecha").as(String.class);

                Join<RegistroMaterialUtilizado, Reserva> reserva = root.join("reserva", JoinType.LEFT);
                Join<RegistroMaterialUtilizado, Material> material = root.join("material", JoinType.LEFT);

                // rojo ford focus
                String[] searchParam = filter.split(" ");
                for (int i = 0; i < searchParam.length; i++) {
                    List<Predicate> predicates = new ArrayList<Predicate>();

                    predicates.add(builder.like(nombre, "%" + searchParam[i] + "%"));
                    predicates.add(builder.like(cantidad, "%" + searchParam[i] + "%"));
                    predicates.add(builder.like(fecha, "%" + searchParam[i] + "%"));

                    predicates.add(builder.like(reserva.get("id").as(String.class), searchParam[i]));
                    predicates.add(builder.like(material.<String>get("nombre"), "%" + searchParam[i] + "%"));
                    // predicates.add(builder.like(RegistroMaterialUtilizado.<String>get("total"), "%" + searchParam[i] + "%"));

                    ors.add(builder.or(predicates.toArray(new Predicate[] {})));
                }
                Predicate result = builder.and(ors.toArray(new Predicate[] {}));
                return result;
            }
        };
    }
}
