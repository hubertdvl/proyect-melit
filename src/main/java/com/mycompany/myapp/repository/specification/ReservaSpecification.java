package com.mycompany.myapp.repository.specification;

import com.mycompany.myapp.domain.Cliente;
import com.mycompany.myapp.domain.Instalacion;
import com.mycompany.myapp.domain.Reserva;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ReservaSpecification extends JpaSpecificationExecutor<Reserva> {
    public static Specification<Reserva> searchingParam(String filter) {
        return new Specification<Reserva>() {
            private static final long serialVersionUID = 1L;

            public Predicate toPredicate(Root<Reserva> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                query.distinct(true);
                List<Predicate> ors = new ArrayList<Predicate>();

                Expression<String> fechaInicio = root.get("fechaInicio").as(String.class);
                Expression<String> fechaFin = root.get("fechaFin").as(String.class);
                Expression<String> tipoPago = root.get("tipoPago").as(String.class);
                Expression<String> total = root.get("total").as(String.class);

                Join<Reserva, Instalacion> instalacion = root.join("instalacion", JoinType.LEFT);
                Join<Reserva, Cliente> cliente = root.join("cliente", JoinType.LEFT);

                // rojo ford focus
                String[] searchParam = filter.split(" ");
                for (int i = 0; i < searchParam.length; i++) {
                    List<Predicate> predicates = new ArrayList<Predicate>();

                    predicates.add(builder.like(fechaInicio, "%" + searchParam[i] + "%"));
                    predicates.add(builder.like(fechaFin, "%" + searchParam[i] + "%"));
                    predicates.add(builder.like(tipoPago, "%" + searchParam[i] + "%"));
                    predicates.add(builder.like(total, "%" + searchParam[i] + "%"));

                    predicates.add(builder.like(instalacion.<String>get("nombre"), "%" + searchParam[i] + "%"));
                    predicates.add(builder.like(cliente.<String>get("nombre"), "%" + searchParam[i] + "%"));
                    // predicates.add(builder.like(Reserva.<String>get("total"), "%" + searchParam[i] + "%"));

                    ors.add(builder.or(predicates.toArray(new Predicate[] {})));
                }
                Predicate result = builder.and(ors.toArray(new Predicate[] {}));
                return result;
            }
        };
    }
}
