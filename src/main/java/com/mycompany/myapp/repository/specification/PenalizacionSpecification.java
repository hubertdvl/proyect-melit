package com.mycompany.myapp.repository.specification;

import com.mycompany.myapp.domain.Cliente;
import com.mycompany.myapp.domain.Penalizacion;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PenalizacionSpecification extends JpaSpecificationExecutor<Penalizacion> {
    public static Specification<Penalizacion> searchingParam(String filter) {
        return new Specification<Penalizacion>() {
            private static final long serialVersionUID = 1L;

            public Predicate toPredicate(Root<Penalizacion> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                query.distinct(true);
                List<Predicate> ors = new ArrayList<Predicate>();

                Expression<String> motivo = root.get("motivo").as(String.class);
                Expression<String> totalAPagar = root.get("totalAPagar").as(String.class);

                Join<Penalizacion, Cliente> cliente = root.join("cliente", JoinType.LEFT);

                // rojo ford focus
                String[] searchParam = filter.split(" ");
                for (int i = 0; i < searchParam.length; i++) {
                    List<Predicate> predicates = new ArrayList<Predicate>();

                    predicates.add(builder.like(motivo, "%" + searchParam[i] + "%"));
                    predicates.add(builder.like(totalAPagar, "%" + searchParam[i] + "%"));

                    predicates.add(builder.like(cliente.<String>get("nombre"), "%" + searchParam[i] + "%"));
                    // predicates.add(builder.like(Penalizacion.<String>get("total"), "%" + searchParam[i] + "%"));

                    ors.add(builder.or(predicates.toArray(new Predicate[] {})));
                }
                Predicate result = builder.and(ors.toArray(new Predicate[] {}));
                return result;
            }
        };
    }
}
