package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Reserva;
import java.time.Instant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Reserva entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReservaRepository extends JpaRepository<Reserva, Long>, JpaSpecificationExecutor<Reserva> {
    @Query("SELECT r FROM Reserva r where r.fechaInicio between :inicio and :fin or r.fechaFin between :inicio and :fin")
    Page<Reserva> findByFecha(@Param("inicio") Instant inicio, @Param("fin") Instant fin, Pageable pageable);

    @Query("SELECT r FROM Reserva r where r.fechaInicio between :inicio and :fin")
    Page<Reserva> findFromToday(@Param("inicio") Instant inicio, @Param("fin") Instant fin, Pageable pageable);
}
