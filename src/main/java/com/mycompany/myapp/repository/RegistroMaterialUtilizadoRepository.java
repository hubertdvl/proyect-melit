package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.RegistroMaterialUtilizado;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RegistroMaterialUtilizado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegistroMaterialUtilizadoRepository
    extends JpaRepository<RegistroMaterialUtilizado, Long>, JpaSpecificationExecutor<RegistroMaterialUtilizado> {
    @Query("SELECT r FROM RegistroMaterialUtilizado r where r.reserva.id = :id")
    Optional<List<RegistroMaterialUtilizado>> findByReserva(@Param("id") Long id);
}
