package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Penalizacion;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Penalizacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PenalizacionRepository extends JpaRepository<Penalizacion, Long>, JpaSpecificationExecutor<Penalizacion> {
    @Query("SELECT p FROM Penalizacion p where p.cliente.id = :id")
    Optional<List<Penalizacion>> findByCliente(@Param("id") Long id);
}
