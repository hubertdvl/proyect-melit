import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs, { Dayjs } from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IReserva, Reserva } from '../reserva.model';
import { ReservaService } from '../service/reserva.service';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { ClienteService } from 'app/entities/cliente/service/cliente.service';
import { IInstalacion } from 'app/entities/instalacion/instalacion.model';
import { InstalacionService } from 'app/entities/instalacion/service/instalacion.service';
import { IMaterial, Material } from 'app/entities/material/material.model';
import { MaterialService } from 'app/entities/material/service/material.service';
import { IPenalizacion, Penalizacion } from 'app/entities/penalizacion/penalizacion.model';
import { PenalizacionService } from 'app/entities/penalizacion/service/penalizacion.service';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-reserva-update',
  templateUrl: './reserva-update.component.html',
})
export class ReservaUpdateComponent implements OnInit {
  hasId = false;
  showCantidadReservada = false;
  checkbox: null | boolean = null;
  isSaving = true;
  totalPrice = 0;

  //Obtener el día actual en formato ngbDate
  currentDate = new Date();
  currentDay = this.currentDate.getDate();
  ngbCurrentDateStruct = { day: this.currentDay, month: this.currentDate.getUTCMonth() + 1, year: this.currentDate.getUTCFullYear() };

  //dia inicio y fin de la reserva

  startDayReserve: NgbDateStruct | undefined;
  finalDayReserve: NgbDateStruct | undefined;

  //horario

  hoursArray = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
  hoursEndReserve: number[] = [];

  //hora inicio y final reserva

  startHourReserve = '';
  endHourReserve = '';

  //fecha reservas en formato date

  startDateReserve: Date | undefined;
  endDateReserve: Date | undefined;

  //fecha en dayjs

  startDayjs: Dayjs | undefined;
  endDayjs: Dayjs | undefined;

  //habilitar/deshabilitar selects

  hourStartEnable = true;
  dateEndEnabble = true;
  hourEndEnable = true;

  //valores formulario

  startHourValue = '';
  endHourValue = '';
  clienteValue = '';
  instalacionValue = '';

  clientesSharedCollection: ICliente[] = [];
  instalacionsSharedCollection: IInstalacion[] = [];
  materialSharedCollection: IMaterial[] = [];
  penalizacionSharedCollection: IPenalizacion[] = [];

  objectSelectedInstalation = <IInstalacion>{};
  objectSelectedClient = <ICliente>{};

  editForm = this.fb.group({
    id: [],
    fechaInicio: [],
    horaInicio: [],
    fechaFin: [],
    horaFin: [],
    tipoPago: [],
    total: [],
    cliente: [],
    instalacion: [],
    materiales: this.fb.array([]),
    registrosId: [],
  });

  // Creamos el control del formulario dinamico
  control = <FormArray>this.editForm.controls['materiales'];

  constructor(
    protected reservaService: ReservaService,
    protected clienteService: ClienteService,
    protected materialService: MaterialService,
    protected instalacionService: InstalacionService,
    protected penalizacionService: PenalizacionService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder,
    private calendar: NgbCalendar
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ reserva }) => {
      if (reserva.id === undefined) {
        const today = dayjs().startOf('day');
        reserva.fechaInicio = today;
        reserva.fechaFin = today;
      }

      if (reserva.id !== undefined) {
        this.showCantidadReservada = true;
        this.checkbox = true;
        this.hasId = true;
        if (reserva.instalacion) {
          this.objectSelectedInstalation.id = reserva.instalacion.id;
          this.getSelectedOjbInstalation(this.objectSelectedInstalation);
        }
        // this.getMaterials(reserva.instalacion.id);
      }

      this.updateForm(reserva);

      this.loadRelationshipsOptions();
      this.editForm.get('horaInicio')!.disable();
      this.editForm.get('horaFin')!.disable();
    });
  }

  // Funcion para añadir campos en el formulario dinamico
  patch(): void {
    this.materialSharedCollection.forEach(material => {
      this.control.push(
        this.patchValues(material.id, material.nombre, material.cantidadReservada, material.cantidadDisponible, material.instalacion)
      );
    });
  }

  // funcion que retorna un nuevo formArray dependiendo del contenido de materialSharedCollection
  patchValues(id: any, nombre: any, cantidadReservada: any, cantidadDisponible: any, instalacion: any): AbstractControl {
    if (this.showCantidadReservada) {
      return this.fb.group({
        id: [id],
        nombre: [nombre],
        cantidadReservada: [cantidadReservada, [Validators.max(cantidadDisponible), Validators.min(0)]],
        cantidadDisponible: [cantidadDisponible],
        instalacion: [instalacion],
      });
    } else {
      return this.fb.group({
        id: [id],
        nombre: [nombre],
        cantidadReservada: [0, [Validators.max(cantidadDisponible), Validators.min(0)]],
        cantidadDisponible: [cantidadDisponible],
        instalacion: [instalacion],
      });
    }
  }

  getControls(): any {
    return (this.editForm.get('materiales') as FormArray).controls;
  }

  // Funcion para obtener los materiales dependiendo de la instalacion elegida
  // getMaterials(instalacionId: any): void {
  //   this.materialService
  //     .searchByInstalacion(instalacionId)
  //     .pipe(map((res: HttpResponse<IMaterial[]>) => res.body ?? []))
  //     .pipe(
  //       map((material: IMaterial[]) =>
  //         this.materialService.addMaterialToCollectionIfMissing(material, this.editForm.get('materiales')!.value)
  //       )
  //     )
  //     .subscribe((material: IMaterial[]) => (this.materialSharedCollection = material));

  //   if (this.checkbox) {
  //     for (let i = this.control.length - 1; i >= 0; i--) {
  //       this.control.removeAt(i);
  //     }
  //     this.checkbox = null;
  //   }
  // }

  getPenalizaciones(clienteId: number): void {
    this.penalizacionService
      .findPenalizacionesByCliente(clienteId)
      .pipe(map((res: HttpResponse<IPenalizacion[]>) => res.body ?? []))
      .pipe(
        map((penalizacion: IPenalizacion[]) =>
          this.penalizacionService.addPenalizacionToCollectionIfMissing(penalizacion, this.editForm.get('materiales')!.value)
        )
      )
      .subscribe((penalizacion: IPenalizacion[]) => (this.penalizacionSharedCollection = penalizacion));
  }

  // Funcion para mostrar o ocultar el campo de materiales dependiendo de la eleccion del usuario
  toggle(event: any): void {
    if (event.target.checked) {
      this.checkbox = true;
      this.patch();
      if (this.hasId) {
        this.showCantidadReservada = false;
      }
    } else {
      for (let i = this.control.length - 1; i >= 0; i--) {
        this.control.removeAt(i);
      }
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.validateFormNulls();

    const reserva = this.createFromForm();
    reserva.fechaInicio = this.startDayjs;
    reserva.fechaFin = this.endDayjs;
    reserva.total = this.totalPrice;
    if (reserva.id !== undefined) {
      this.subscribeToSaveResponse(this.reservaService.update(reserva));
    } else {
      this.subscribeToSaveResponse(this.reservaService.create(reserva));
    }
  }

  getSelectedOjbInstalation(obj: object): void {
    this.objectSelectedInstalation = obj;
    this.materialService
      .searchByInstalacion(this.objectSelectedInstalation.id!)
      .pipe(map((res: HttpResponse<IMaterial[]>) => res.body ?? []))
      .pipe(
        map((material: IMaterial[]) =>
          this.materialService.addMaterialToCollectionIfMissing(material, this.editForm.get('materiales')!.value)
        )
      )
      .subscribe((material: IMaterial[]) => (this.materialSharedCollection = material));

    if (this.checkbox) {
      for (let i = this.control.length - 1; i >= 0; i--) {
        this.control.removeAt(i);
      }
      this.checkbox = null;
    }

    this.validateFormNulls();
  }

  getSelectedOjbCliente(obj: object): void {
    this.objectSelectedClient = obj;
    this.validateFormNulls();
  }

  calculateTotalPrice(): any {
    if (this.objectSelectedClient.edad && this.objectSelectedInstalation.precioPorHora) {
      const price = this.objectSelectedInstalation.precioPorHora;
      const edad = this.objectSelectedClient.edad;
      let discountPrice = 0;

      if (edad <= 16 || edad >= 45) {
        discountPrice = price * 0.3;
        this.totalPrice = price - discountPrice;
        return this.totalPrice;
      } else if (edad <= 24 && edad >= 17) {
        discountPrice = price * 0.15;
        this.totalPrice = price - discountPrice;
        return this.totalPrice;
      } else {
        return 0;
      }
    }
  }

  setMinReserveDate(date: NgbDateStruct): void {
    this.startDayReserve = date;
    this.editForm.get('horaInicio')!.enable();
    this.hourStartEnable = false;

    if (this.finalDayReserve != null) {
      this.checkSameDate();
    }
  }

  setEndReserveDate(date: NgbDateStruct): void {
    this.finalDayReserve = date;

    this.checkSameDate();
    this.editForm.get('horaFin')!.enable();
    this.hourEndEnable = false;
  }

  //Ver si es la misma fecha para cambiar las horas
  checkSameDate(): void {
    const hourStart = parseInt(this.startHourReserve, 10);
    this.hoursEndReserve = [...this.hoursArray];

    this.startDateReserve = new Date(this.startDayReserve!.year, this.startDayReserve!.month, this.startDayReserve!.day);
    this.endDateReserve = new Date(this.finalDayReserve!.year, this.finalDayReserve!.month, this.finalDayReserve!.day);

    //Horas proximas de inicio de la reserva

    const same = this.startDateReserve.getTime() === this.endDateReserve.getTime();
    if (same) {
      for (let i = 0; i < this.hoursEndReserve.length; i++) {
        if (hourStart === this.hoursEndReserve[i]) {
          this.hoursEndReserve.splice(0, i + 1);
        }
      }
    }
    this.validateFormNulls();
  }

  getStartHourSelected(hour: any): void {
    this.startHourReserve = hour.target.value;

    //Dactivar campo en función de si ha elegido hora o no
    if (this.startHourReserve !== 'null') {
      this.dateEndEnabble = false;
    } else {
      this.dateEndEnabble = true;
      this.editForm.get('horaFin')!.disable();
    }

    this.validateFormNulls();
  }

  getEndHourSelected(hour: any): void {
    this.endHourReserve = hour.target.value;
    // this.validateFormNulls();
    this.parseTimeReserve();

    // if (fechaInicio !== undefined && fechaFin !== undefined) {
    const fechas = {
      fechaInicio: this.reservaService.convertDateToJSON(this.startDayjs!),
      fechaFin: this.reservaService.convertDateToJSON(this.endDayjs!),
    };

    this.instalacionService
      .findAvailable(fechas)
      .pipe(map((res: HttpResponse<IInstalacion[]>) => res.body ?? []))
      .pipe(
        map((instalacions: IInstalacion[]) =>
          this.instalacionService.addInstalacionToCollectionIfMissing(instalacions, this.editForm.get('instalacion')!.value)
        )
      )
      .subscribe((instalacions: IInstalacion[]) => (this.instalacionsSharedCollection = instalacions));
  }

  //parsear la fecha a formato dayjs
  parseTimeReserve(): void {
    if (this.startDayReserve && this.finalDayReserve) {
      if (this.startHourReserve && this.endHourReserve) {
        const hourStart = parseInt(this.startHourReserve, 10);
        const hourEnd = parseInt(this.endHourReserve, 10);
        this.startDateReserve?.setHours(hourStart);
        this.startDateReserve?.setMinutes(0);
        this.endDateReserve?.setHours(hourEnd);
        this.endDateReserve?.setMinutes(0);

        this.startDayjs = dayjs(this.startDateReserve);
        this.endDayjs = dayjs(this.endDateReserve);
      }
    }
  }

  //validacion campos vacíos formulario
  validateFormNulls(): void {
    if (this.startHourReserve !== '' && this.endHourReserve !== '' && this.startHourReserve !== 'null' && this.endHourReserve !== 'null') {
      if (this.objectSelectedClient.id && this.objectSelectedInstalation.id) {
        this.isSaving = false;
      } else {
        this.isSaving = true;
      }
    } else {
      this.isSaving = true;
    }

    this.objectSelectedClient;
    this.objectSelectedInstalation;
  }

  trackClienteById(index: number, item: ICliente): number {
    return item.id!;
  }

  trackInstalacionById(index: number, item: IInstalacion): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReserva>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(reserva: IReserva): void {
    this.editForm.patchValue({
      id: reserva.id,
      fechaInicio: this.startDayjs,
      horaInicio: this.startHourReserve,
      fechaFin: this.endDayjs,
      horaFin: this.endHourReserve,
      tipoPago: reserva.tipoPago,
      total: reserva.total,
      cliente: reserva.cliente,
      instalacion: reserva.instalacion,
      registrosId: reserva.registrosId,
    });

    this.clientesSharedCollection = this.clienteService.addClienteToCollectionIfMissing(this.clientesSharedCollection, reserva.cliente);
    this.instalacionsSharedCollection = this.instalacionService.addInstalacionToCollectionIfMissing(
      this.instalacionsSharedCollection,
      reserva.instalacion
    );
  }

  protected loadRelationshipsOptions(): void {
    this.clienteService
      .query()
      .pipe(map((res: HttpResponse<ICliente[]>) => res.body ?? []))
      .pipe(
        map((clientes: ICliente[]) => this.clienteService.addClienteToCollectionIfMissing(clientes, this.editForm.get('cliente')!.value))
      )
      .subscribe((clientes: ICliente[]) => (this.clientesSharedCollection = clientes));
  }

  protected createFromForm(): IReserva {
    return {
      ...new Reserva(),
      id: this.editForm.get(['id'])!.value,
      fechaInicio: this.editForm.get(['fechaInicio'])!.value
        ? dayjs(this.editForm.get(['fechaInicio'])!.value, DATE_TIME_FORMAT)
        : undefined,
      fechaFin: this.editForm.get(['fechaFin'])!.value ? dayjs(this.editForm.get(['fechaFin'])!.value, DATE_TIME_FORMAT) : undefined,
      tipoPago: this.editForm.get(['tipoPago'])!.value,
      total: this.editForm.get(['total'])!.value,
      cliente: this.editForm.get(['cliente'])!.value,
      instalacion: this.editForm.get(['instalacion'])!.value,
      materiales: this.editForm.get(['materiales'])!.value,
      registrosId: this.editForm.get(['registrosId'])!.value,
    };
  }
}
