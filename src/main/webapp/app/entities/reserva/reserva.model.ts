import dayjs from 'dayjs/esm';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { IInstalacion } from 'app/entities/instalacion/instalacion.model';
import { IMaterial } from '../material/material.model';
import { IRegistroMaterialUtilizado } from '../registro-material-utilizado/registro-material-utilizado.model';

export interface IReserva {
  id?: number;
  fechaInicio?: dayjs.Dayjs | null;
  horaInicio?: number | null;
  fechaFin?: dayjs.Dayjs | null;
  horaFin?: number | null;
  tipoPago?: string | null;
  total?: number | null;
  cliente?: ICliente | null;
  instalacion?: IInstalacion | null;
  materiales?: IMaterial[] | null;
  registrosId?: [] | null;
}

export class Reserva implements IReserva {
  constructor(
    public id?: number,
    public fechaInicio?: dayjs.Dayjs | null,
    public fechaFin?: dayjs.Dayjs | null,
    public tipoPago?: string | null,
    public total?: number | null,
    public cliente?: ICliente | null,
    public instalacion?: IInstalacion | null,
    public materiales?: IMaterial[] | null,
    public registrosId?: [] | null
  ) {}
}

export function getReservaIdentifier(reserva: IReserva): number | undefined {
  return reserva.id;
}
