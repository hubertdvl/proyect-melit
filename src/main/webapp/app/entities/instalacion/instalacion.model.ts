export interface IInstalacion {
  id?: number;
  nombre?: string | null;
  precioPorHora?: number | null;
}

export class Instalacion implements IInstalacion {
  constructor(public id?: number, public nombre?: string | null, public precioPorHora?: number | null) {}
}

export function getInstalacionIdentifier(instalacion: IInstalacion): number | undefined {
  return instalacion.id;
}
