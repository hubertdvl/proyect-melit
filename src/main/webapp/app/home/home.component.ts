import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';

import { ReservaService } from '../entities/reserva/service/reserva.service';
import { IReserva } from '../entities/reserva//reserva.model';
import dayjs, { Dayjs } from 'dayjs/esm';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  isNavbarCollapsed = true;
  reservaSharedCollection: IReserva[] = [];
  startOfDay = dayjs().startOf('day');
  // endOfDay = dayjs().endOf('day');

  private readonly destroy$ = new Subject<void>();

  constructor(private accountService: AccountService, private router: Router, private reservaService: ReservaService) {}

  ngOnInit(): void {
    const fechas = {
      fechaInicio: this.reservaService.convertDateToJSON(dayjs().startOf('day').add(1, 'month')),
      fechaFin: this.reservaService.convertDateToJSON(dayjs().endOf('day').add(1, 'month')),
    };

    this.accountService
      .getAuthenticationState()
      .pipe(takeUntil(this.destroy$))
      .subscribe(account => (this.account = account));

    this.reservaService
      .findReservasFromToday(fechas)
      .pipe(map((res: HttpResponse<IReserva[]>) => res.body ?? []))
      .subscribe((reserva: IReserva[]) => (this.reservaSharedCollection = reserva));

    const x = this.startOfDay.add(1, 'month');
    const t = x;
  }

  formatDate(date: Dayjs): string {
    const formatedDate = dayjs(date).format('HH:mm');
    return formatedDate;
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }
}
